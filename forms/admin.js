/**
 * The functions in this file handle some of the admin form interactive gui.
 * Makes the interface for selecting lists in this modules admin form similar
 * to selecting lists in the actual StreamSend Interface.
 */

function StreamSendAdminDisableListOption(id,which){
  if(id.constructor.toString().indexOf("Array") == -1){id = [id];}
  sObj = $('#edit-list-select')[0];
  sObj.selectedIndex = 0;
  for(var i=0;i < sObj.options.length;i++){
    for(var j=0;j<id.length;j++){
      if(sObj.options[i].value == id[j]){
        sObj.options[i].disabled = (which==undefined)?true:which;
      }
    }
  }
}

function StreamSendAdminRemoveList(sObj,id){
  if(sObj.value=='remove'){
    StreamSendAdminDisableListOption(id,false);
    $('#list_option_'+id).remove();
  }
}
